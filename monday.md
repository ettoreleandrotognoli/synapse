# Cronograma

Unknown

- [BD 101 23/09/2019](#BD-101-23092019)
- [01 Começando com BD 30/09/2019](#01-Comecando-com-BD-30092019)
- [02 Começando com BD 07/10/2019](#02-Comecando-com-BD-07102019)
- [BD um pouco mais aprofundado - 14/10/2019](#BD-um-pouco-mais-aprofundado-14102019)
- [Unknown 21/10/2019](#unknown-21102019)
- [Unknown 28/10/2019](#unknown-28102019)
- [Unknown 04/11/2019](#unknown-04112019)
- [Unknown 11/11/2019](#unknown-11112019)
- [API 18/11/2019](#API-18112019)
- [Unknown 25/11/2019](#unknown-25112019)
- [Unknown 02/12/2019](#unknown-02122019)

## BD 101 23/09/2019
- Explicação de BD, instalação de uma imagem do docker com mysql e phpmyadmin OU workbench

## 01 Começando com BD 30/09/2019
- Modelagem de algo simples

## 02 Começando com BD 07/10/2019
- Algumas queries e conceitos

## BD um pouco mais aprofundado 14/10/2019
- Inner join e outros joins

## Unknown 21/10/2019

## Unknown 28/10/2019

## Unknown 04/11/2019

## API 11/11/2019

## Unknown 18/11/2019

## Unknown 25/11/2019

## Unknown 02/12/2019
