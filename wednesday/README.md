# Cronograma

- [Intro 18/09/2019](#intro-18092019)
- [Lógica Computacional 25/09/2019](#lógica-computacional-25092019)
- [Arquitetura de Computadores 02/10/2019](#arquitetura-de-computadores-02102019)
- [Programação Imperativa 09/10/2019](#programação-imperativa-09102019)
- [Ponteiros e Referências 16/10/2019](#ponteiros-e-referências-16102019)
- [Recursividade e Complexidade 23/10/2019](#recursividade-e-complexidade-23102019)
- [Programação Orientada a Objetos 30/10/2019](#programação-orientada-a-objetos-30102019)
- [Programação Funcional 06/11/2019](#programação-funcional-06112019)
- [Tipos Genéricos 13/11/2019](#tipos-genéricos-13112019)
- [Unknown 20/11/2019](#unknown-20112019)
- [Unknown 27/11/2019](#unknown-27112019)
- [Unknown 04/12/2019](#unknown-04122019)

## Intro 18/09/2019

[Brain Storm dos temas](./brainstorm.md)

Projeto no GIT

## [Lógica Computacional 25/09/2019](./logica-computacional/README.md)

- Algebra Booleana
- Soma de Produtos e Produto de Somas
- Axiomas
- Mapa de Karnaugh

## Arquitetura de Computadores 02/10/2019

- Circuitos Lógicos
  - Memoria
  - Multiplexador
  - Contador
  - ULA
- Assembly
  - MIPS

## Programação Imperativa 09/10/2019

Linguagens

- C
- Python

Assuntos

- Tipos de Dados
- Controle de Fluxo
- Estruturas de Repetição
- Funções

## Ponteiros e Referências 16/10/2019

Linguagens

- C/C++
- Python

Assuntos

- Ponteiros & Referências
- Listas Encadeadas

## Recursividade e Complexidade 23/10/2019

Linguagens

- C/C++
- Python

Assuntos

- Funções Recursivas
- Árvores Binárias
- Análise de Complexidade

## Programação Orientada a Objetos 30/10/2019

Linguagens

- Java
- C++
- Python

## Programação Funcional 06/11/2019

Linguagens

- Lisp
- Python
- Java
- JavaScript

## Tipos Genéricos 13/11/2019

Linguagens

- Python
- Java
- TypeScript
- C++
- PHP

Assuntos

- Type Hints

## Unknown 20/11/2019

## Unknown 27/11/2019

## Unknown 04/12/2019
